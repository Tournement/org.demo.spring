/**
 * 
 */
package org.demo.spring.launcher;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

import org.demo.spring.model.Account;
import org.demo.spring.model.IAccount;
import org.demo.spring.model.IOperation;
import org.demo.spring.model.ModelIOImpl;
import org.demo.spring.model.Operation;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author jorda
 *
 */
class LauncherTest {

    /**
     * @throws java.lang.Exception
     */
    @BeforeAll
    static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterAll
    static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @BeforeEach
    void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterEach
    void tearDown() throws Exception {
    }

    @Test
    void test() throws IOException {
	Account account = new Account();
	account.setOwnerName("toto");
	List<IOperation> linkedList = new LinkedList<>();
	Operation operation = new Operation();
	operation.setDueDate(Instant.now());
	operation.setLabel("payment");
	operation.setValue(-33);
	linkedList.add(operation);
	operation = new Operation();
	operation.setDueDate(Instant.now());
	operation.setLabel("salary");
	operation.setValue(2000);
	linkedList.add(operation);
	account.setOperation(linkedList);
	ModelIOImpl modelIO = new ModelIOImpl();
	FileOutputStream fileOutputStream = new FileOutputStream("test.jordanou");
	modelIO.writeIAccount(fileOutputStream, account);
	FileInputStream fileInputStream = new FileInputStream("test.jordanou");
	IAccount account2 = modelIO.readIAccount(fileInputStream);
	System.out.println("meurs");
    }
}
