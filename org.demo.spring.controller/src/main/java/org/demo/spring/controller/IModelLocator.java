package org.demo.spring.controller;

import java.io.IOException;
import java.io.InputStream;

public interface IModelLocator {

    InputStream getInputStream() throws IOException;

}
