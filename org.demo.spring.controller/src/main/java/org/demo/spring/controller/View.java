package org.demo.spring.controller;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.AbstractTableModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy
public class View implements IView {
    private static final DateTimeFormatter Formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
	    .withLocale(Locale.FRANCE).withZone(ZoneId.systemDefault());
    @Autowired
    private ModelController modelController;
    private AbstractTableModel tableModel;

    @PostConstruct
    private void display() {
	buildJFrame().setVisible(true);
    }

    private JFrame buildJFrame() {
	JFrame frame = new JFrame("Account of " + getModelController().getOwnerName());
	tableModel = new AbstractTableModel() {

	    private static final long serialVersionUID = 1L;

	    @Override
	    public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
		    return Formatter.format(getModelController().getOperationDueDate(rowIndex));
		case 1:
		    return getModelController().getOperationValue(rowIndex);
		case 2:
		    return getModelController().getOperationLabel(rowIndex);
		default:
		    return null;
		}
	    }

	    @Override
	    public int getRowCount() {
		return getModelController().getOperationCount();
	    }

	    @Override
	    public int getColumnCount() {
		return 3;
	    }
	};
	JTable table = new JTable(tableModel);
	JScrollPane comp = new JScrollPane(table);
	comp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	comp.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	frame.add(comp);
	frame.pack();
	return frame;
    }

    @Override
    public ModelController getModelController() {
	return modelController;
    }

    @Override
    public void update() {
	tableModel.fireTableStructureChanged();
    }

}
