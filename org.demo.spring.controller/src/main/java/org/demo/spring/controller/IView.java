package org.demo.spring.controller;

public interface IView {
    ModelController getModelController();

    default void destroy() {
	getModelController().destroy(this);
    }

    void update();
}
