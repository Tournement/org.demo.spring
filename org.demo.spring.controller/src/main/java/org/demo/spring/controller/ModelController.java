package org.demo.spring.controller;

import java.io.IOException;
import java.time.Instant;
import java.util.LinkedList;

import javax.annotation.PostConstruct;

import org.demo.spring.model.IAccount;
import org.demo.spring.model.ModelIO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy
public class ModelController {
    @Autowired
    private ApplicationContext context;

    @Autowired
    ModelIO modelIO;

    @Autowired(required = false)
    IModelLocator locator;

    private IAccount account;

    private LinkedList<IView> view = new LinkedList<>();

    @PostConstruct
    private void loadModel() throws IOException {
	account = modelIO.readIAccount(locator.getInputStream());
    }

    public IView createView() {
	IView bean = context.getBean(IView.class);
	view.add(bean);
	return bean;
    }

    void destroy(IView iView) {
	view.remove(iView);
    }

    public String getOwnerName() {
	return account.getOwnerName();
    }

    public int getOperationCount() {
	return account.getOperations().size();
    }

    public Instant getOperationDueDate(int i) {
	return account.getOperations().get(i).getDueDate();
    }

    public String getOperationLabel(int i) {
	return account.getOperations().get(i).getLabel();
    }

    public int getOperationValue(int i) {
	return account.getOperations().get(i).getValue();
    }
}
