package org.demo.spring.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy
public class FileModelLocator implements IModelLocator {
    private String filePath;

    public FileModelLocator(String filePath) {
	super();
	this.filePath = filePath;
    }

    @Override
    public InputStream getInputStream() throws FileNotFoundException {
	return new FileInputStream(new File(filePath).getAbsolutePath());
    }

}
