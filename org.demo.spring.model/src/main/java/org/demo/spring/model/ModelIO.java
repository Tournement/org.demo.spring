package org.demo.spring.model;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface ModelIO {

	IAccount readIAccount(InputStream input) throws IOException;

	void writeIAccount(OutputStream output, IAccount account) throws IOException;

}
