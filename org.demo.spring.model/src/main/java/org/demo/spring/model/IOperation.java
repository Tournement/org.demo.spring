package org.demo.spring.model;

import java.time.Instant;

public interface IOperation {

    Instant getDueDate();

    void setDueDate(Instant dueDate);

    int getValue();

    void setValue(int value);

    String getLabel();

    void setLabel(String label);

}