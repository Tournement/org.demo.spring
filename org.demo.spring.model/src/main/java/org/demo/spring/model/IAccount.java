package org.demo.spring.model;

import java.util.List;

public interface IAccount {

    String getOwnerName();

    void setOwnerName(String ownerName);

    List<IOperation> getOperations();

    void setOperation(List<IOperation> operations);

}