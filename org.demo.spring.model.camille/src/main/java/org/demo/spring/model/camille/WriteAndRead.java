package org.demo.spring.model.camille;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.demo.spring.model.IAccount;
import org.demo.spring.model.IOperation;
import org.demo.spring.model.ModelIO;
import org.junit.jupiter.api.Test;

class WriteAndRead {

    @Test
    void test() throws FileNotFoundException, IOException {
	ModelIO model = new ModelIOImpl();

	List<IOperation> operations = new ArrayList<>();
	IAccount account = new Account();
	account.setOwnerName("Michel");

	IOperation operation1 = new Operation();
	operation1.setDueDate(Instant.now());
	operation1.setLabel("Salaire");
	operation1.setValue(1000);
	operations.add(operation1);

	IOperation operation2 = new Operation();
	operation2.setDueDate(Instant.now());
	operation2.setLabel("McDo");
	operation2.setValue(-20);
	operations.add(operation2);

	account.setOperation(operations);

	model.writeIAccount(new FileOutputStream("C:/Users/camil/Desktop/Michel.txt"), account);

	model.readIAccount(new FileInputStream("C:/Users/camil/Desktop/Michel.txt"));
	System.out.println();
    }

}
