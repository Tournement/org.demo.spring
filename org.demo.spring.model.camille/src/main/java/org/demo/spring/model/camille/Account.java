package org.demo.spring.model.camille;

import java.util.List;

import org.demo.spring.model.IAccount;
import org.demo.spring.model.IOperation;

public class Account implements IAccount {

    String ownerName;
    List<IOperation> operations;

    @Override
    public String getOwnerName() {
	return ownerName;
    }

    @Override
    public void setOwnerName(String ownerName) {
	this.ownerName = ownerName;
    }

    @Override
    public List<IOperation> getOperations() {
	return operations;
    }

    @Override
    public void setOperation(List<IOperation> operations) {
	this.operations = operations;
    }

}
