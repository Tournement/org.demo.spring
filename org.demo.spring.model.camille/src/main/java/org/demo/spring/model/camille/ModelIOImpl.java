package org.demo.spring.model.camille;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.demo.spring.model.IAccount;
import org.demo.spring.model.IOperation;
import org.demo.spring.model.ModelIO;
import org.springframework.stereotype.Service;

@Service("csvModel")
public class ModelIOImpl implements ModelIO {

    @Override
    public IAccount readIAccount(InputStream input) throws IOException {
	byte[] buffer = new byte[1024];
	StringBuilder accountLine = new StringBuilder();
	int readed;
	while ((readed = input.read(buffer)) > 0) {
	    accountLine.append(new String(buffer, 0, readed, Charset.forName("UTF-8")));
	}

	String[] accountLineElements = accountLine.toString().split((";"));
	IAccount account = new Account();
	account.setOwnerName(accountLineElements[0]);

	List<IOperation> operations = new ArrayList<>();
	IOperation operation = new Operation();

	for (int i = 1; i < accountLineElements.length; i++) {
	    String[] operationElements = accountLineElements[i].split("\\|");
	    operation.setDueDate(Instant.parse(operationElements[0]));
	    operation.setValue(Integer.parseInt(operationElements[1]));
	    operation.setLabel(operationElements[2]);
	    operations.add(operation);
	    operation = new Operation();
	}

	account.setOperation(operations);

	return account;

    }

    @Override
    public void writeIAccount(OutputStream output, IAccount account) throws IOException {
	StringBuilder accountLine = new StringBuilder();
	accountLine.append(account.getOwnerName());
	accountLine.append(";");
	for (IOperation operation : account.getOperations()) {
	    accountLine.append(operation);
	    accountLine.append(";");
	}
	String subAccountLine = accountLine.toString();
	output.write(subAccountLine.substring(0, subAccountLine.length() - 1).getBytes(Charset.forName("UTF-8")));
    }

}
