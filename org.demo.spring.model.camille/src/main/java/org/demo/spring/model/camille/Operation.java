package org.demo.spring.model.camille;

import java.time.Instant;

import org.demo.spring.model.IOperation;

public class Operation implements IOperation {

    Instant dueDate;
    int value;
    String label;

    @Override
    public Instant getDueDate() {
	return dueDate;
    }

    @Override
    public void setDueDate(Instant dueDate) {
	this.dueDate = dueDate;
    }

    @Override
    public int getValue() {
	return value;
    }

    @Override
    public void setValue(int value) {
	this.value = value;
    }

    @Override
    public String getLabel() {
	return label;
    }

    @Override
    public void setLabel(String label) {
	this.label = label;
    }

    @Override
    public String toString() {
	return dueDate + "|" + value + "|" + label;
    }

}
