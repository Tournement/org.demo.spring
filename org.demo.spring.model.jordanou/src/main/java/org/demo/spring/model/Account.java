/**
 * 
 */
package org.demo.spring.model;

import java.util.List;

/**
 * @author jorda
 *
 */
public class Account implements IAccount {

    String name;
    List<IOperation> operationList;

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IAccount#getOwnerName()
     */
    @Override
    public String getOwnerName() {
	return name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IAccount#setOwnerName(java.lang.String)
     */
    @Override
    public void setOwnerName(String ownerName) {
	name = ownerName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IAccount#getOperations()
     */
    @Override
    public List<IOperation> getOperations() {
	return operationList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IAccount#setOperation(java.util.List)
     */
    @Override
    public void setOperation(List<IOperation> operations) {
	this.operationList = operations;
    }

}
