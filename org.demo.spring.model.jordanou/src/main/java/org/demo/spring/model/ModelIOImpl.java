/**
 * 
 */
package org.demo.spring.model;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

/**
 * @author jorda
 *
 */
@Component
public class ModelIOImpl implements ModelIO {

    @Override
    public IAccount readIAccount(InputStream input) throws IOException {
	Account account = new Account();
	Workbook wb = new XSSFWorkbook(input);
	Sheet sheetAt = wb.getSheetAt(0);
	account.setOwnerName(sheetAt.getSheetName());
	Row row;
	List<IOperation> operations = new LinkedList<>();
	int i = 0;
	while ((row = sheetAt.getRow(i)) != null) {
	    Operation operation = new Operation();
	    Cell cell = row.getCell(0);
	    operation.setDueDate(Instant.parse(cell.getStringCellValue()));
	    cell = row.getCell(1);
	    operation.setLabel(cell.getStringCellValue());
	    cell = row.getCell(2);
	    operation.setValue((int) cell.getNumericCellValue());
	    operations.add(operation);
	    i++;
	}
	account.setOperation(operations);
	wb.close();
	return account;
    }

    @Override
    public void writeIAccount(OutputStream output, IAccount account) throws IOException {
	Workbook wb = new XSSFWorkbook();

	Sheet sheet = wb.createSheet(account.getOwnerName());
	List<IOperation> operations = account.getOperations();
	for (int i = 0; i < operations.size(); i++) {
	    Row row = sheet.createRow(i);
	    Cell createCell = row.createCell(0);
	    createCell.setCellType(CellType.STRING);
	    createCell.setCellValue(operations.get(i).getDueDate().toString());
	    createCell = row.createCell(1);
	    createCell.setCellType(CellType.STRING);
	    createCell.setCellValue(operations.get(i).getLabel().toString());
	    createCell = row.createCell(2);
	    createCell.setCellType(CellType.NUMERIC);
	    createCell.setCellValue(new Double(operations.get(i).getValue()));
	}
	// Write the output to a file
	wb.write(output);
	wb.close();
    }

}
