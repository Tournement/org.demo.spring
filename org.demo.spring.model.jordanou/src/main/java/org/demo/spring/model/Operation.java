/**
 * 
 */
package org.demo.spring.model;

import java.time.Instant;

/**
 * @author jorda
 *
 */
public class Operation implements IOperation {

    private Instant dueDate;
    private int i;
    private String label;

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IOperation#getDueDate()
     */
    @Override
    public Instant getDueDate() {
	return dueDate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IOperation#setDueDate(java.time.Instant)
     */
    @Override
    public void setDueDate(Instant dueDate) {
	this.dueDate = dueDate;

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IOperation#getValue()
     */
    @Override
    public int getValue() {
	return i;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IOperation#setValue(int)
     */
    @Override
    public void setValue(int value) {
	i = value;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IOperation#getLabel()
     */
    @Override
    public String getLabel() {
	return label;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IOperation#setLabel(java.lang.String)
     */
    @Override
    public void setLabel(String label) {
	this.label = label;
    }

}
