package org.demo.spring.model.xml;

import java.time.Instant;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.demo.spring.model.IOperation;

import com.migesok.jaxb.adapter.javatime.InstantXmlAdapter;

public class Operation implements IOperation {
    Instant dueDate;

    int value;
    String label;

    public Operation() {
	// do nothing
    }

    public Operation(IOperation operation) {
	this();
	this.dueDate = operation.getDueDate();
	this.value = operation.getValue();
	this.label = operation.getLabel();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IOperation#getDueDate()
     */
    @Override
    @XmlJavaTypeAdapter(InstantXmlAdapter.class)
    public Instant getDueDate() {
	return dueDate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IOperation#setDueDate(java.time.Instant)
     */
    @Override
    public void setDueDate(Instant dueDate) {
	this.dueDate = dueDate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IOperation#getValue()
     */
    @Override
    @XmlElement
    public int getValue() {
	return value;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IOperation#setValue(int)
     */
    @Override
    public void setValue(int value) {
	this.value = value;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IOperation#getLabel()
     */
    @Override
    @XmlElement
    public String getLabel() {
	return label;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IOperation#setLabel(java.lang.String)
     */
    @Override
    public void setLabel(String label) {
	this.label = label;
    }
}
