package org.demo.spring.model.xml;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.demo.spring.model.IAccount;
import org.demo.spring.model.ModelIO;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service("xmlModel")
@Lazy
public class ModelXMLIO implements ModelIO {

    @Override
    public IAccount readIAccount(InputStream input) throws IOException {
	try {
	    JAXBContext jaxbContext = JAXBContext.newInstance(Operation.class, Account.class);
	    Unmarshaller jaxbMarshaller = jaxbContext.createUnmarshaller();

	    // output pretty printed
	    return (IAccount) jaxbMarshaller.unmarshal(input);

	} catch (JAXBException e) {
	    throw new IOException("jaxb fail to read xml", e);
	}
    }

    @Override
    public void writeIAccount(OutputStream output, IAccount account) throws IOException {
	try {
	    JAXBContext jaxbContext = JAXBContext.newInstance(Operation.class, Account.class);
	    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

	    // output pretty printed
	    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	    jaxbMarshaller.marshal(account, output);
	} catch (JAXBException e) {
	    throw new IOException("jaxb fail to read xml", e);
	}
    }

}
