package org.demo.spring.model.xml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.demo.spring.model.IAccount;
import org.demo.spring.model.IOperation;

@XmlRootElement
public class Account implements IAccount {
    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IAccount#getOwnerName()
     */
    @Override
    @XmlAttribute
    public String getOwnerName() {
	return ownerName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IAccount#setOwnerName(java.lang.String)
     */
    @Override
    public void setOwnerName(String ownerName) {
	this.ownerName = ownerName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IAccount#getOperations()
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<IOperation> getOperations() {
	return (List<IOperation>) (List<?>) operations;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.demo.spring.model.IAccount#setOperation(java.util.List)
     */
    @Override
    public void setOperation(List<IOperation> operations) {
	this.operations = new ArrayList<>();
	for (IOperation operation : operations) {
	    this.operations.add(new Operation(operation));
	}
    }

    String ownerName;

    @XmlElement
    List<Operation> operations;

}
