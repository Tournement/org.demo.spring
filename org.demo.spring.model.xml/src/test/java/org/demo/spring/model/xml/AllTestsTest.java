package org.demo.spring.model.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;

import org.demo.spring.model.IAccount;
import org.demo.spring.model.ModelIO;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

class AllTestsTest {
    private static Account account;
    private static AnnotationConfigApplicationContext context;

    @BeforeAll
    public static void init() {
	context = new AnnotationConfigApplicationContext("org.demo.spring");
	account = new Account();
	account.ownerName = "toto";
	account.operations = new LinkedList<>();
	Operation operation = new Operation();
	operation.dueDate = Instant.now().minus(4, ChronoUnit.DAYS);
	operation.label = "payment";
	operation.value = -33;
	account.operations.add(operation);
	operation = new Operation();
	operation.dueDate = Instant.now().minus(8, ChronoUnit.DAYS);
	operation.label = "salary";
	operation.value = 2000;
	account.operations.add(operation);
    }

    @Test
    void test() throws BeansException, FileNotFoundException, IOException {
	ModelIO modelio = context.getBean(ModelIO.class);
	File file = new File("test.xml");
	if (!file.exists()) {
	    file.createNewFile();
	}
	modelio.writeIAccount(new FileOutputStream(file), account);
	IAccount loadedAccount = modelio.readIAccount(new FileInputStream("test.xml"));
	modelio.writeIAccount(System.out, loadedAccount);
    }

}
